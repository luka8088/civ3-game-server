<?php

function main () {

  require_once __dir__ . '/yaml/Inline.php';
  require_once __dir__ . '/yaml/Parser.php';
  require_once __dir__ . '/yaml/Yaml.php';

  parseArgs();
  runThinVNC();
  runCiv3();
  handleSaveFiles();

}

function parseArgs () {

  if (isset($_SERVER['argv'][1]) && $_SERVER['argv'][1] == '--config') {
    if (!isset($_SERVER['argv'][2]) || !is_file($_SERVER['argv'][2]))
      throw new \Exception('Config file `' . $_SERVER['argv'][2] . '` not found.');
    $_SERVER = array_merge($_SERVER, parse_ini_file($_SERVER['argv'][2]));
  }

  if (!isset($_SERVER['tempPath']))
    $_SERVER['tempPath'] = __dir__ . '/temp';

  if (!isset($_SERVER['dataPath']))
    $_SERVER['dataPath'] = __dir__ . '/data';

}

function runThinVNC () {

  echo 'Running ThinVNC...';

  file_put_contents(__dir__ . '/ThinVNC/ThinVnc.ini', preg_replace('/(?<=\n)[ \t]+/s', '', '
    [Authentication]
    Unicode=0
    User=' . parse_url($_SERVER['webURL'], PHP_URL_USER) . '
    Password=' . parse_url($_SERVER['webURL'], PHP_URL_PASS) . '
    Type=Digest
    [Http]
    Port=' . (parse_url($_SERVER['webURL'], PHP_URL_PORT) ? parse_url($_SERVER['webURL'], PHP_URL_PORT) : '80') . '
    Enabled=1
    [Tcp]
    Port=
    [General]
    AutoStart=1
  '));

  //pclose(popen('start /B ' . __dir__ . '/ThinVNC/ThinVnc.exe 1>NUl 2>NUL', 'r'));
  $process = proc_open(__dir__ . '/ThinVNC/ThinVnc.exe', [['pipe', 'r'], ['pipe', 'w'], ['pipe', 'w']], $pipes);

  echo " ok, available at {$_SERVER['webURL']}\n";

}

function runCiv3 () {

  echo 'Running Civ3...';

  file_put_contents($_SERVER['tempPath'] . '/runciv3.bat', trim(preg_replace('/(?<=\n)[ \t]+/s', '', "
    :loop
    cd \"C:\Program Files (x86)\Firaxis Games\Sid Meier's Civilization III Complete\Conquests\"
    Civ3Conquests.exe
    goto loop
  ")));

  $process = proc_open(
    'start cmd /C "' . $_SERVER['tempPath'] . '/runciv3.bat"',
    [['pipe', 'r'], ['pipe', 'w'], ['pipe', 'w']],
    $pipes
  );

  echo " ok\n";

}

function handleSaveFiles () {

  while (true) {

    $database = \Symfony\Component\Yaml\Yaml::parse(file_get_contents($_SERVER['dataPath'] . '/database.yaml'));

    $state = [];
    if (is_file($_SERVER['dataPath'] . '/state.json'))
      $state = json_decode(file_get_contents($_SERVER['dataPath'] . '/state.json'), true);

    $savesPath = getenv('LOCALAPPDATA')
      . '/VirtualStore/Program Files (x86)/Firaxis Games/Sid Meier\'s Civilization III Complete/Conquests/Saves/';

    foreach ($database['game'] as $gameID => $game) {

      $turnInfo = civ3GetTurnInfo($savesPath . '/' . $game['file']);

      if (!isset($state[$gameID]['playerTurn']))
        $state[$gameID]['playerTurn'] = $turnInfo['nextPlayer'];

      if ($state[$gameID]['playerTurn'] != $turnInfo['nextPlayer'])
        foreach ((array) $database['player'][$turnInfo['nextPlayer']]['email'] as $playerEmail)
          sendEmail(
            $_SERVER['emailFrom'],
            $playerEmail,
            'Your turn in ' . $gameID . '!',
            "{$turnInfo['previousPlayer']} made his/her mode, {$turnInfo['nextPlayer']} is next!\n"
            . "Play your turn here: {$_SERVER['webURL']}"
          );

      $state[$gameID]['playerTurn'] = $turnInfo['nextPlayer'];

    }

    file_put_contents($_SERVER['dataPath'] . '/state.json', json_encode($state));

    sleep(60);

  }

}

function civ3GetTurnInfo ($saveFile) {

  if (!is_dir($_SERVER['tempPath'] . '/savexpnd'))
    mkdir($_SERVER['tempPath'] . '/savexpnd', 0777, true);

  $contents = file_get_contents($saveFile);

  if (strpos($contents, 'CIV3') !== 0) {
    file_put_contents($_SERVER['tempPath'] . '/savexpnd/__compressed.tmp', $contents);
    $exe = __dir__ . '/savexpnd/savexpnd.exe';
    $in = $_SERVER['tempPath'] . '/savexpnd/__compressed.tmp';
    $out = $_SERVER['tempPath'] . '/savexpnd/__uncompressed.tmp';
    //`savexpnd.exe __compressed.tmp __uncompressed.tmp ""`;
    shell_exec("$exe $in $out \"\"");
    $contents = file_get_contents($_SERVER['tempPath'] . '/savexpnd/__uncompressed.tmp');
    unlink($_SERVER['tempPath'] . '/savexpnd/__compressed.tmp');
    unlink($_SERVER['tempPath'] . '/savexpnd/__uncompressed.tmp');
  }

  $data = substr($contents, strpos($contents, 'CNSL') + 4);
  $index = ord($data[4]);

  preg_match_all('/(?s)LEAD.{4324}(.*?)\0/', $contents, $matches, PREG_SET_ORDER);
  $previousPlayer = $matches[$index][1];
  $nextPlayer = strlen($matches[$index + 1][1]) > 0 ? $matches[$index + 1][1] : $matches[1][1];

  return [
    'previousPlayer' => $previousPlayer,
    'nextPlayer' => $nextPlayer,
  ];

}

function sendEmail ($from, $to, $subject, $body) {

  if (!is_dir($_SERVER['tempPath'] . '/sendmail'))
    mkdir($_SERVER['tempPath'] . '/sendmail', 0777, true);

  file_put_contents($_SERVER['tempPath'] . '/sendmail/mail.mail', trim(preg_replace('/(?<=\n)[ \t]+/s', '', "
    From: $from
    To: $to
    Subject: $subject

    $body
  ")));

  file_put_contents(__dir__ . '/sendmail/sendmail.ini', preg_replace('/(?<=\n)[ \t]+/s', '', "
    [sendmail]
    smtp_server={$_SERVER['smtp_server']}
    smtp_port={$_SERVER['smtp_port']}
    smtp_ssl={$_SERVER['smtp_ssl']}
    auth_username={$_SERVER['smtp_auth_username']}
    auth_password={$_SERVER['smtp_auth_password']}
  "));

  shell_exec(__dir__ . '\sendmail\sendmail.exe -t < ' . $_SERVER['tempPath'] . '/sendmail/mail.mail');

}

main();
